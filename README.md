## Mega2 Bitbucket repository ##

The latest development snapshot of the Mega2 code can
be obtained from this Bitbucket repository.  Please note
that this development snapshot is not as thoroughly tested
as our stable release version, but does contain the newest
features and changes.

To obtain the stable release version (including pre-compiled binaries), please go to
<https://watson.hgen.pitt.edu/register>.

## Documentation ##

Please see the Mega2 documentation, which is available in this
distribution as a PDF in the mega2_html folder:

>mega2_html/Mega2_Documentation.pdf

It is also available online as html <https://watson.hgen.pitt.edu/docs/mega2_html/mega2.html> and as a pdf <https://watson.hgen.pitt.edu/docs/mega2_html/Mega2_Documentation.pdf>.


## A note about Mega2 versions available via git: ##
  
  + The master branch reflects our latest working code; **it may contain bugs.**

  + You can download source code for the stable release by clicking on 'Source' and then set the tag to the latest (highest) number, and then clicking the clone button.