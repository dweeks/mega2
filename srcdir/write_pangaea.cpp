/*
  Mega2: Manipulation Environment for Genetic Analysis.

  Copyright 2012-2019, University of Pittsburgh. All Rights Reserved.

  Contributors to Mega2: Robert Baron, Justin R. Stickel, Charles P. Kollar,
  Nandita Mukhopadhyay, Lee Almasy, Mark Schroeder, William P. Mulvihill,
  and Daniel E. Weeks.

  This file is part of the Mega2 program, which is free software; you
  can redistribute it and/or modify it under the terms of the GNU
  General Public License as published by the Free Software Foundation;
  either version 3 of the License, or (at your option) any later
  version.

  Mega2 is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  For further information contact:
      Daniel E. Weeks
      e-mail: weeks@pitt.edu

===========================================================================
*/


#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "common.h"
#include "typedefs.h"

#include "loop.h"
#include "sh_util.h"

#include "batch_input_ext.h"
#include "error_messages_ext.h"
#include "fcmap_ext.h"
#include "genetic_utils_ext.h"
#include "linkage_ext.h"
#include "omit_ped_ext.h"
#include "output_file_names_ext.h"
#include "output_routines_ext.h"
#include "user_input_ext.h"
#include "utils_ext.h"

#include "write_pangaea_ext.h"

/*
     error_messages_ext.h:  mssgf my_calloc warnf
              fcmap_ext.h:  fcmap
            linkage_ext.h:  get_loci_on_chromosome get_loci_on_chromosomes get_unmapped_loci
           omit_ped_ext.h:  omit_peds
  output_file_names_ext.h:  CHR_STR change_output_chr create_mssg file_status print_outfile_mssg
    output_routines_ext.h:  create_formats field_widths
         user_input_ext.h:  individual_id_item pedigree_id_item test_modified
              utils_ext.h:  EXIT draw_line script_time_stamp summary_time_stamp
*/

static void inner_file_names(char **file_names, const char *num, const char *stem = "pangaea");


static void save_PANGAEA_peds(linkage_ped_top *Top, char *file_names[],
                              const int pwid, const int fwid, const int subopt)
{
    vlpCLASS(save_peds,trait,ped_per) {
     vlpCTOR(save_peds,trait,ped_per) { }
        typedef char *str;
        str *file_names;
        int subopt;
        int skip;

        void file_loop() {
            mssgvf("        PANGAEA pedigree file:      %s/%s\n", *_opath, file_names[0]);
            data_loop(*_opath, file_names[0], "w");
        }
        void file_header() {
            int nquant = 0, nint = 0, col = 5;

            if (_Top->IndivCnt > 20000)
                pr_printf("allow pedigree size %d\n", _Top->IndivCnt);
            pr_printf("input pedigree size %d\n", _Top->IndivCnt);
            pr_printf("input pedigree record father mother\n");
            pr_printf("input pedigree record gender present\n");
            if (_ttraitp == 0) {
            } else if (_ttraitp->Type == AFFECTION && _ttraitp->Pheno->Props.Affection.ClassCnt > 1) {
                pr_printf("# file columns %d,%d (integer pair) correspond to trait name %s\n",
                          col, col+1,  _ttraitp->LocusName);
                col += 2;
                nint += 2;
            } else if (_ttraitp->Type == AFFECTION) {
                pr_printf("# file column %d (integer) corresponds to trait name %s\n",
                          col,  _ttraitp->LocusName);
                col++;
                nint++;
            } else if (_ttraitp->Type == QUANT) {
                pr_printf("# file column %d (real) corresponds to trait name %s\n",
                          nquant+1+nint+1,  _ttraitp->LocusName);
                nquant++;
            }
            pr_printf("input pedigree record names 3 integers %d", nint + 1 /* sex */);
            if (nquant)
                pr_printf(" reals %d", nquant);
            pr_nl();

            pr_printf("****************************************\n");
        }
        void ped_start() {
            if (subopt == 6) {
/*
                if (_tpedtreep->EntryCnt > 50 || _ped > 100) {
                    warnvf("Skipping family %s; too many families for analysis\n",_tpedtreep->Name);
                    skip = 1;
                } else
*/
                    skip = 0;
            } else
                skip = 0;
        }
        void inner() {
            if (skip) return;
            pr_per();
            pr_parent();
            pr_sex();
            if (_ttraitp == 0) {
            } else if (_ttraitp->Type == AFFECTION) {
/*
                int ase;
                ase = aff_status_entry(_tpersonp->Data[_trait].Affection.Status,
                                       _tpersonp->Data[_trait].Affection.Class,
                                       &(_Top->LocusTop->Locus[_trait]));
                pr_printf("%1d   ", ase);
*/
                pr_printf("%1d   ", _tpersonp->Pheno[_trait].Affection.Status);
                if (_ttraitp->Pheno->Props.Affection.ClassCnt > 1)
                    pr_printf("%1d   ", _tpersonp->Pheno[_trait].Affection.Class);
            } else if (_ttraitp->Type == QUANT) {
                if (fabs(_tpersonp->Pheno[_trait].Quant - MissingQuant) <= EPSILON) {
                    pr_printf("%10.5f ",  999.0);
                } else {
                    pr_printf("%10.5f ", _tpersonp->Pheno[_trait].Quant);
                }
            } else {
                pr_printf("%d ", 0);
            }
            pr_nl();
        }
    } *sp = new save_peds(Top);

    sp->file_names = file_names;
    sp->subopt     = subopt;
    sp->load_formats(fwid, pwid, -1);
    sp->iterate();
    delete sp;

    vlpCLASS(save_peds_all,both,ped_per_trait) {
     vlpCTOR(save_peds_all,both,ped_per_trait) { }
        typedef char *str;
        str *file_names;

        void file_loop() {
            mssgvf("        PANGAEA pedigree file:      %s/%s\n", *_opath, file_names[9]);
            data_loop(*_opath, file_names[9], "w");
        }
        void file_header() {
            int i, j;
            int nquant = 0, nint = 0;

            if (_Top->IndivCnt > 20000)
                pr_printf("allow pedigree size %d\n", _Top->IndivCnt);
            pr_printf("input pedigree size %d\n", _Top->IndivCnt);
            pr_printf("input pedigree record father mother\n");
            pr_printf("input pedigree record gender present\n");
            int col = 5;
            for (i = 0; i < num_traits; i++) {
                j = global_trait_entries[i];
                if (j == -1)  continue;
                linkage_locus_rec *loc = &_LTop->Locus[j];
                if (loc->Type == AFFECTION && loc->Pheno->Props.Affection.ClassCnt > 1) {
                    pr_printf("# file columns %d,%d (integer pair) correspond to trait name %s\n",
                              col, col+1,  _LTop->Locus[j].LocusName);
                    col += 2;
                    nint += 2;
                } else if (loc->Type == AFFECTION) {
                    pr_printf("# file column %d (integer) corresponds to trait name %s\n",
                              col,  _LTop->Locus[j].LocusName);
                    col++;
                    nint++;
                } else if (loc->Type == QUANT) {
                    pr_printf("# file column %d (real) corresponds to trait name %s\n",
                              nquant+1+nint+1,  _LTop->Locus[j].LocusName);
                    nquant++;
                }
            }
            pr_printf("input pedigree record names 3 integers %d", nint + 1 /* sex */);
            if (nquant)
                pr_printf(" reals %d", nquant);
            pr_nl();

            pr_printf("****************************************\n");
        }
        void per_start() {
            pr_per();
            pr_parent();
            pr_sex();
        }
        void per_end() {
            pr_nl();
        }
        void inner() {
            if (_ttraitp == 0) {
            } else if (_ttraitp->Type == AFFECTION) {
/*
                int ase;
                ase = aff_status_entry(_tpersonp->Data[_trait].Affection.Status,
                                       _tpersonp->Data[_trait].Affection.Class,
                                       &(_Top->LocusTop->Locus[_trait]));
                pr_printf("%1d   ", ase);
*/
                pr_printf("%1d   ", _tpersonp->Pheno[_trait].Affection.Status);
                if (_ttraitp->Pheno->Props.Affection.ClassCnt > 1)
                    pr_printf("%1d   ", _tpersonp->Pheno[_trait].Affection.Class);
            } else if (_ttraitp->Type == QUANT) {
                if (fabs(_tpersonp->Pheno[_trait].Quant - MissingQuant) <= EPSILON) {
                    pr_printf("%10.5f ",  999.0);
                } else {
                    pr_printf("%10.5f ", _tpersonp->Pheno[_trait].Quant);
                }
            } else {
                pr_printf("%d ", 0);
            }
        }
        void sort_quant_last() // first names; then integer values; then real values
        {
            int i = 0, j = 0;
            int *straits = CALLOC(num_traits, int);
            for (i = 0; i < num_traits; i++)
                if (_LTop->Locus[i].Type != QUANT)
                    straits[j++] = global_trait_entries[i];
            for (i = 0; i < num_traits; i++)
                if (_LTop->Locus[i].Type == QUANT)
                    straits[j++] = global_trait_entries[i];
            for (i = 0; i < num_traits; i++)
                global_trait_entries[i] = straits[i];
            free(straits);
        }
    } ;
/*
   *sp1 = new save_peds_all(Top);

    sp1->file_names = file_names;
    sp1->load_formats(fwid, pwid, -1);
    sp1->sort_quant_last();
    sp1->iterate();
    delete sp1;
*/

}

/**
   @param output_format determines how the files are written:
*/
const char *pgm_path[] = {"",
                          "PedComp/pedcheck",    //1
                          "PedComp/kin",         //2
                          "PedComp/translink",   //3
                          "Lodscore/lm_linkage", //4
                          "Lodscore/lm_bayes",   //5
                          "Autozyg/lm_ibdtests", //6
                          "Autozyg/lm_ibdtests", //7
};

static void write_PANGAEA_sh(linkage_ped_top *Top, char *file_names[], char *pgm, int subopt)
{
    int top_shell = (LoopOverChrm && main_chromocnt > 1) || (LoopOverTrait && num_traits > 1) ||
        strcmp(output_paths[0], ".");

    dataloop::sh_exec *sh = 0;
    if (top_shell) {
        sh = new dataloop::sh_exec(Top);
        sh->filep_open(output_paths[0], file_names[4], "w");
        sh->sh_main();
    }

    vlpCLASS(PANGAEA_sh_script,both,sh_exec) {
     vlpCTOR(PANGAEA_sh_script,both,sh_exec) {
            strcpy(pfx, (LoopOverTrait && num_traits > 1) ? "../" : "");
        }
        typedef char *str;
        str *file_names;
        dataloop::sh_exec *sh;
        char *pgm;
        int subopt;
        char pfx[4];

        void file_loop() {
            mssgvf("        PANGAEA shell file:         %s/%s\n", *_opath, file_names[3]);
            data_loop(*_opath, file_names[3], "w");
        }
        void file_header() {
            if (sh)
                sh->sh_sh(this);
            sh_shell_type();
            sh_id();
            script_time_stamp(_filep);
#ifdef RUNSHELL_SETUP
	    // This handles the environment variable setup to allow the checking
	    // functions in 'batch_run' to work correctly...
	    fprintf_env_checkset_csh(_filep, "_PANGAEA", "pangaea");
#endif /* RUNSHELL_SETUP */
        }
        void inner () {
            char cmd[2*FILENAME_LENGTH];
            char target[FILENAME_LENGTH];

            sprintf(target, "%spar_chr_trt", file_names[5]);
            sh_need_data(pgm, target);

            sprintf(cmd, "cat %spar_chr_trt %s.par_trt",
//                    file_names[5], pgm);
                    file_names[5], file_names[6]);

            sprintf(target, "%spar", file_names[5]);
            sh_cat(cmd, target);

            sprintf(cmd, "%s/%s", "$MORGAN_RELEASE", pgm_path[subopt]);
            sh_find_pgm(cmd, pgm_path[subopt]);

            sprintf(cmd+strlen(cmd), " %s", target);
            strcat(target, ".log");
            sh_rm(target);
            sh_run(pgm, cmd, target);
            sh_show(pgm, target);

            fprintf_status_check_csh(_filep, "pangaea", 0);

            if (subopt == 3) { // translink
                sprintf(target, "datafile%s", file_names[11]);
                sh_save_output(pgm, "datafile.dat", target);
                sprintf(target, "pedfile%s", file_names[11]);
                sh_save_output(pgm, "pedfile.dat", target);
            }
        }
        void sh_find_pgm(const char *fullpath, const char *path) {
            pr_printf("if ( $?MORGAN_RELEASE  ) then\n");
            pr_printf("  set morgan_def=1\n");
            pr_printf("else\n");
            pr_printf("  set morgan_def=0\n");
            pr_printf("  set MORGAN_RELEASE=MORGAN_RELEASE\n");
            pr_printf("endif\n");
            pr_printf("echo\n");
            pr_printf("if (! $morgan_def  || ! -x \"%s\" ) then\n", fullpath);
            pr_printf("  echo The %s executable was not found - \n", fullpath);
            pr_printf("  echo please set your MORGAN_RELEASE environment variable properly so %s can be found.\n", pgm);
            pr_printf("  echo\n");
            pr_printf("    if (! $morgan_def) then\n");
            pr_printf("      echo MORGAN_RELEASE is not defined.\n");
            pr_printf("    else\n");
            pr_printf("      echo MORGAN_RELEASE is $MORGAN_RELEASE.\n");
            pr_printf("    endif\n");
            pr_printf("  echo\n");
            pr_printf("  echo If using Bash and ksh you would use something like this:\n");
            pr_printf("  echo export MORGAN_RELEASE=path_to/MORGAN_V311_RELEASE\n");
            pr_printf("  echo\n");
            pr_printf("  echo If using csh you would use something like this:\n");
            pr_printf("  echo setenv MORGAN_RELEASE path_to/MORGAN_V311_RELEASE\n");
            pr_printf("  echo\n");
            pr_printf("  echo \"Be sure to run 'make %s' to build %s in the %s\"\n",
                      pgm, pgm, path);
            pr_printf("  echo sub directory of MORGAN_RELEASE.\n");
            pr_printf("  echo\n");
            pr_printf("  echo \"For further details, please see 'PANGAEA/MORGAN' section of the Mega2 documentation.\"\n");
            pr_printf("  exit 0\n");
            pr_printf("endif\n");
            pr_nl();
        }
    } *xp = new PANGAEA_sh_script(Top);

    xp->file_names = file_names;
    xp->sh         = sh;
    xp->pgm        = pgm;
    xp->subopt     = subopt;
    xp->iterate();
    delete xp;

    if (top_shell) {
        mssgvf("        PANGAEA top shell file:     %s/%s\n", output_paths[0], file_names[4]);
        mssgvf("                the above shell runs all shells\n");
        sh->filep_close();
        delete sh;
    }
}

vlpCLASS(par_var,both,null) {
 vlpCTOR(par_var,both,null) {
        strcpy(pfx, (LoopOverTrait && num_traits > 1) ? "../" : "");
    }
    typedef char *str;
    str *file_names;
    char pfx[4];

    void file_loop() {
        mssgvf("        PANGAEA chr var  par file:  %s/%s\n", *_opath, file_names[7]);
        data_loop(*_opath, file_names[7], "w");
    }
    void inner () {
        pr_printf("input pedigree file '%s'\n", file_names[0]);
        pr_printf("output overwrite pedigree file '%s.out'\n", file_names[0]);
        pr_printf("# chromosome data does not depend on traits so is in the above directory\n");
        pr_printf("input marker data file '%s%s'\n", pfx, file_names[1]);
        pr_nl();
        if (_ttraitp != 0) {
            if (_ttraitp->Type == AFFECTION && _ttraitp->Pheno->Props.Affection.ClassCnt > 1) {
                pr_printf("input extra file '%s.liability.extra'\n", _ttraitp->LocusName);
                pr_printf("input pedigree record trait 1 integer pairs 2 3\n");
                pr_printf("set trait 1 data discrete with liability\n");
            } else if (_ttraitp->Type == AFFECTION) {
                pr_printf("input pedigree record trait 1 integer 2\n");
                pr_printf("set trait 1 data discrete\n");
            } else if (_ttraitp->Type == QUANT) {
                pr_printf("input pedigree record trait 1 real 1\n");
                pr_printf("set trait 1 data quant\n");
            }
        }
    }
};

vlpCLASS(liability_traits,trait,null) {
 vlpCTOR(liability_traits,trait,null) { }
    typedef char *str;
    str *file_names;

    void file_loop() {
        if (_ttraitp == 0 || _ttraitp->Type != AFFECTION || _ttraitp->Pheno->Props.Affection.ClassCnt <= 1)
            return;
        char outfl[FILENAME_LENGTH];
        sprintf(outfl, "%s.liability.extra", _ttraitp->LocusName);
        mssgvf("        PANGAEA liability file:     %s/%s\n", *_opath, outfl);
        data_loop(*_opath, outfl, "w");
    }
    void inner() {
        int i = 0;
        int cnt  = _ttraitp->Pheno->Props.Affection.ClassCnt;
        linkage_affection_class *pen = _ttraitp->Pheno->Props.Affection.Class; //.{Male/Female/Auto}Pen[0]@3 MaleDef

        for (i = 1; i <= cnt; i++) {
            pr_printf("%d %.6f %.6f %.6f\n",
                      i,
                      pen->AutoPen[0],
                      pen->AutoPen[1],
                      pen->AutoPen[2]);
            pen++;
        }
    }
};

/**
   @param output_format determines how the files are written:
*/
static void write_PANGAEA_par_template(linkage_ped_top *Top, char *file_names[], char *pgm)
{
    par_var *xp1 = new par_var(Top);
    xp1->file_names     = file_names;
    xp1->iterate();
    delete xp1;

    vlpCLASS(template_par_user,trait,null) {
     vlpCTOR(template_par_user,trait,null) { }
        typedef char *str;
        str *file_names;

        void file_loop() {
            mssgvf("        PANGAEA user par file:      %s/%s\n", *_opath, file_names[10]);
            data_loop(*_opath, file_names[10], "w");
        }
        void inner() {
            pr_printf("set printlevel 5\n");
            pr_printf("output pedigree chronological\n");
            pr_printf("output pedigree record father mother\n");

            pr_printf("set printlevel 4\n");
            pr_printf("select all markers\n");
            pr_printf("select trait 1\n");
            pr_printf("set trait 1 data discrete\n");
            pr_printf("set traits 1 for tlocs 1 incomplete penetrances .05 .90 .90\n");

            pr_printf("set traits 1 tlocs 1\n");
            pr_printf("set tloc 1 allele frequency .09 .91\n");
            pr_printf("map tloc 1 marker 00 recom frac .0005\n");

            pr_printf("use single meiosis sampler\n");
            pr_printf("set MC iterations 1\n");
            pr_printf("set proband gametes 202 0 202 1\n");
        }
    } *xp2 = new template_par_user(Top);
    xp2->file_names     = file_names;
    xp2->iterate(); 
    delete xp2;
}

static void write_PANGAEA_par_pedcheck(linkage_ped_top *Top, char *file_names[], char *pgm)
{

    vlpCLASS(pedcheck_xx_par_var,both,null) {
     vlpCTOR(pedcheck_xx_par_var,both,null) { }
        typedef char *str;
        str *file_names;

        void file_loop() {
            mssgvf("        PANGAEA chr var  par file:  %s/%s\n", *_opath, file_names[7]);
            data_loop(*_opath, file_names[7], "w");
        }
        void inner () {
            pr_printf("input pedigree file '%s'\n", file_names[0]);
            pr_printf("output overwrite pedigree file '%s.out'\n", file_names[0]);
        }
    } *xp1 = new pedcheck_xx_par_var(Top);
    xp1->file_names     = file_names;
    xp1->iterate();
    delete xp1;

    vlpCLASS(pedcheck_par_user,trait,null) {
     vlpCTOR(pedcheck_par_user,trait,null) { }
        typedef char *str;
        str *file_names;

        void file_loop() {
            mssgvf("        PANGAEA user par file:      %s/%s\n", *_opath, file_names[10]);
            data_loop(*_opath, file_names[10], "w");
        }
        void inner() {
            pr_printf("set printlevel 5\n");
            pr_printf("output pedigree chronological\n");
            pr_printf("output pedigree record father mother\n");

        }
    } *xp2 = new pedcheck_par_user(Top);
    xp2->file_names     = file_names;
    xp2->iterate(); 
    delete xp2;
}

static void write_PANGAEA_par_kin(linkage_ped_top *Top, char *file_names[], char *pgm)
{
    vlpCLASS(kin_xx_par_varA,both,ped_per) {
     vlpCTOR(kin_xx_par_varA,both,ped_per) { }
        typedef char *str;
        str *file_names;
        int i;

        void file_loop() {
            i = 0;
            mssgvf("        PANGAEA chr var  par file:  %s/%s\n", *_opath, file_names[7]);
            data_loop(*_opath, file_names[7], "w");
        }
        void file_header () {
            pr_printf("input pedigree file '%s'\n", file_names[0]);
            pr_printf("output overwrite pedigree file '%s.out'\n", file_names[0]);
            pr_nl();
        }
        void ped_start() {
            ++i;
            if (_tpedtreep->EntryCnt == 1) pr_printf("# ");
            pr_printf("compute component %d kinship coeff ", i);
        }
        void inner() {
            int nper;
            for (nper = _per+1; nper < _Top->Ped[_ped].EntryCnt; nper++) {
                pr_per();
                pr_per(&_tpedtreep->Entry[nper]);
            }
        }
        void ped_end() {
            pr_nl();
        }
        void file_trailer() {
            pr_nl();
        }
    } *xp1A = new kin_xx_par_varA(Top);
    xp1A->file_names     = file_names;
    xp1A->load_formats(5, 5, -1);   //?
    xp1A->iterate();
    delete xp1A;

    vlpCLASS(kin_xx_par_varB,both,ped_per) {
     vlpCTOR(kin_xx_par_varB,both,ped_per) { }
        typedef char *str;
        str *file_names;
        int i;

        void file_loop() {
            i = 0;
            mssgvf("        PANGAEA chr var  par file:  %s/%s\n", *_opath, file_names[7]);
            data_loop(*_opath, file_names[7], "a");
        }
        void ped_start() {
            pr_printf("compute component %d inbreeding coeff ", ++i);
        }
        void inner() {
            pr_per();
        }
        void ped_end() {
            pr_nl();
        }
        void file_trailer() {
            pr_nl();
        }
    } *xp1B = new kin_xx_par_varB(Top);
    xp1B->file_names     = file_names;
    xp1B->load_formats(5, 5, -1);   //?
    xp1B->iterate();
    delete xp1B;

    vlpCLASS(kin_xx_par_varC,both,ped_per) {
     vlpCTOR(kin_xx_par_varC,both,ped_per) { }
        typedef char *str;
        str *file_names;
        int i;

        void file_loop() {
            i = 0;
            mssgvf("        PANGAEA chr var  par file:  %s/%s\n", *_opath, file_names[7]);
            data_loop(*_opath, file_names[7], "a");
        }
        void ped_start() {
            pr_printf("compute component %d two-locus inbreeding coeff ", ++i);
        }
        void inner() {
            pr_per();
        }
        void ped_end() {
            pr_nl();
        }
        void file_trailer() {
            pr_nl();
        }
    } *xp1C = new kin_xx_par_varC(Top);
    xp1C->file_names     = file_names;
    xp1C->load_formats(5, 5, -1);   //?
    xp1C->iterate();
    delete xp1C;

    vlpCLASS(kin_par_user,trait,null) {
     vlpCTOR(kin_par_user,trait,null) { }
        typedef char *str;
        str *file_names;

        void file_loop() {
            mssgvf("        PANGAEA user par file:      %s/%s\n", *_opath, file_names[10]);
            data_loop(*_opath, file_names[10], "w");
        }
        void inner() {
            pr_printf("set printlevel 5\n");
            pr_printf("# output pedigree chronological\n");

        }
    } *xp2 = new kin_par_user(Top);
    xp2->file_names     = file_names;
    xp2->iterate(); 
    delete xp2;
}

static void write_PANGAEA_par_translink(linkage_ped_top *Top, char *file_names[], char *pgm)
{

    vlpCLASS(translink_xx_par_var,both,null) {
     vlpCTOR(translink_xx_par_var,both,null) {
            strcpy(pfx, (LoopOverTrait && num_traits > 1) ? "../" : "");
        }
        typedef char *str;
        str *file_names;
        char pfx[4];

        void file_loop() {
            if (_ttraitp == 0) {
            } else if (_ttraitp->Type == AFFECTION && _ttraitp->Pheno->Props.Affection.ClassCnt > 1) {
                warnvf("%s: Trait with Liability Class is not supported.  Trait will be ignored.\n", _ttraitp->LocusName);
            } else if (_ttraitp->Type == QUANT) {
                warnvf("%s: QUANT type is not supported.  Trait will be ignored.\n", _ttraitp->LocusName);
            }
            mssgvf("        PANGAEA chr var  par file:  %s/%s\n", *_opath, file_names[7]);
            data_loop(*_opath, file_names[7], "w");
        }
        void inner () {
            if (_ttraitp == 0) {
                return;
            } else if (_ttraitp->Type == AFFECTION && _ttraitp->Pheno->Props.Affection.ClassCnt > 1) {
                return; // leave empty file
            } else if (_ttraitp->Type == QUANT) {
                return;
            }
            pr_printf("input pedigree file '%s'\n", file_names[0]);
            pr_printf("output overwrite pedigree file '%s.out'\n", file_names[0]);
            pr_printf("# chromosome data does not depend on traits so is in the above directory\n");
            pr_printf("input marker data file '%s%s'\n", pfx, file_names[1]);
            if (_ttraitp != 0) {
                if (_ttraitp->Type == AFFECTION && _ttraitp->Pheno->Props.Affection.ClassCnt > 1) {
                    pr_printf("# liability classes are not supported for translink.\n");
                    pr_printf("# input extra file '%s.liability.extra'\n", _ttraitp->LocusName);
                    pr_printf("input pedigree record trait 1 integer pairs 2 3\n");
                    pr_printf("set trait 1 data discrete with liability\n");
                } else if (_ttraitp->Type == AFFECTION) {
                    pr_printf("input pedigree record trait 1 integer 2\n");
                    pr_printf("set trait 1 data discrete\n");
                } else if (_ttraitp->Type == QUANT) {
                    pr_printf("input pedigree record trait 1 real 1\n");
                    pr_printf("set trait 1 data quant\n");
                }
            }
        }
    } *xp1 = new translink_xx_par_var(Top);
    xp1->file_names     = file_names;
    xp1->iterate();
    delete xp1;

    vlpCLASS(translink_par_user,trait,null) {
     vlpCTOR(translink_par_user,trait,null) {
            strcpy(pfx, (LoopOverTrait && num_traits > 1) ? "../" : "");
        }
        typedef char *str;
        str *file_names;
        char pfx[4];

        void file_loop() {
            mssgvf("        PANGAEA user par file:      %s/%s\n", *_opath, file_names[10]);
            data_loop(*_opath, file_names[10], "w");
        }
        void inner() {
            pr_printf("input extra file '%s%s.extra'\n", pfx, file_names[6]);
            pr_printf("set printlevel 4\n");
            pr_printf("select all markers\n");

            pr_printf("select trait 1\n");
            pr_printf("set traits 1 for tlocs 1 incomplete penetrances .05 .90 .90\n");

            pr_printf("set traits 1 tlocs 1\n");
            pr_printf("set tloc 1 allele frequency .09 .91\n");
            pr_printf("map tloc 1 marker 00 recom frac .0005\n");

            pr_printf("use single meiosis sampler\n");
            pr_printf("set MC iterations 1\n");
            pr_printf("set proband gametes 202 0 202 1\n");
        }
    } *xp2 = new translink_par_user(Top);
    xp2->file_names     = file_names;
    xp2->iterate(); 
    delete xp2;

    dataloop::sh_util *Xpgm = new dataloop::sh_util(Top);
    char outfl[FILENAME_LENGTH];
    sprintf(outfl, "%s.extra", file_names[6]);
    Xpgm->filep_open(output_paths[0], outfl, "w");
    mssgvf("        translink extra file:       %s/%s\n", output_paths[0], outfl);
    Xpgm->pr_printf(" 1 1 1 10\n\
# first item: numnam; if 1, output using MORGAN index as name.\n\
#                     if 0, output using MORGAN names\n\
# second item: siblinks; if 1, first-kid, next_patsib, next_matsib links\n\
#                 will be set from Marlist and output ; if 0, not\n\
# third  item: dexit; effective only if siblinks=1;\n\
#                     if 1, output by Marlist order; \n\
#                     if 0, use MORGAN index order\n\
# fourth item:   the number of lod score evaluations\n\
#     The info on the interval\n\
#           is now taken from the  \"map tloc\" statement \n\
#     In the case of first or last interval, the initial 0.5 or 0,\n\
#          and the final value is 0.0 or 0.5\n\
");
    delete Xpgm;
    Xpgm = NULL;
}

static void write_PANGAEA_par_lod(linkage_ped_top *Top, char *file_names[], char *pgm, int subopt)
{
    par_var *xp1 = new par_var(Top);
    xp1->file_names     = file_names;
    xp1->iterate();
    delete xp1;

    vlpCLASS(lod_par_user,trait,null) {
     vlpCTOR(lod_par_user,trait,null) { }
        typedef char *str;
        str *file_names;
        int subopt;

        void file_loop() {
            mssgvf("        PANGAEA user par file:      %s/%s\n", *_opath, file_names[10]);
            data_loop(*_opath, file_names[10], "w");
        }
        void inner() {
            pr_printf("#For default seeds comment out the following lines\n");
            pr_printf("#To save seeds remove comments\n");
            pr_printf("#input seed file         'seedfil'\n");
            pr_printf("#output seed file        'seedfil'\n");
            pr_printf("set printlevel 3\n");

            pr_printf("select all markers\n");
            pr_printf("select trait 1\n");

            pr_printf("set traits 1 for tlocs 11 incomplete penetrances .05 .90 .90\n");
            pr_printf("set trait 1 tloc 11\n");
            pr_printf("map test tloc 11 all interval proportions .1 .3 .5 .7 .9\n");
            pr_printf("map test tloc 11 external recomb fracts .05 .2 .3 .4 .45\n");
            pr_printf("set tloc 11 allele freqs  .95 .05\n");

            if (subopt == 4 || subopt == 5) { // lm_linkage and lm_bayes common
                pr_printf("use single meiosis sampler\n");
                pr_printf("sample by scan\n");
                pr_printf("set L-sampler probability 0.2\n");
                pr_printf("set MC iterations 3000\n");
                pr_printf("set burn-in iterations 150\n");
                pr_printf("check progress MC iterations 500\n");
            } 
            if (subopt == 4) { //lm_bayes
                pr_printf("use locus-by-locus sampling for setup\n");
            } else if (subopt == 5) { //lm_bayes
                pr_printf("use sequential imputation for setup\n");
                pr_printf("use 1000 sequential imputation realizations for setup\n");
                pr_printf("set sequential imputation proposals every 100 iterations\n");
                pr_printf("set burn-in iterations 500\n");
                pr_printf("set pseudo-prior iterations 3000\n");
                pr_printf("compute scores every 10 iterations\n\n");
            }

        }
    } *xp2 = new lod_par_user(Top);
    xp2->file_names     = file_names;
    xp2->subopt         = subopt;
    xp2->iterate(); 
    delete xp2;

    liability_traits *xp3 = new liability_traits(Top);
    xp3->file_names     = file_names;
    xp3->iterate();
    delete xp3;
}

static void write_PANGAEA_par_ibd_tests(linkage_ped_top *Top, char *file_names[], char *pgm, int subopt)
{
    par_var *xp1 = new par_var(Top);
    xp1->file_names     = file_names;
    xp1->iterate();
    delete xp1;

    vlpCLASS(lod_par_user,trait,null) {
     vlpCTOR(lod_par_user,trait,null) { }
        typedef char *str;
        str *file_names;
        int subopt;

        void file_loop() {
            mssgvf("        PANGAEA user par file:      %s/%s\n", *_opath, file_names[10]);
            data_loop(*_opath, file_names[10], "w");
        }
        void inner() {
            pr_printf("#For default seeds comment out the following lines\n");
            pr_printf("#To save seeds remove comments\n");
            pr_printf("#output permutat seeds only\n");
            pr_printf("#output sampler seeds only\n");
            pr_printf("#input seed file         'seedfil'\n");
            pr_printf("#output seed file        'seedfil'\n");
            pr_printf("set printlevel 4\n\n");

            pr_printf("select all markers\n");
            pr_printf("select trait 1\n\n");

            pr_printf("sample by scan\n");
            pr_printf("set L-sampler probability 0.5\n");
            pr_printf("set MC iterations 3000\n");
            pr_printf("#set burn-in iterations 150\n");
            pr_printf("#check progress MC iterations 500\n");
            pr_printf("compute scores every 100 iterations\n");
            if (subopt == 6) { //lm_ibdtests
                pr_printf("compute ibd statistics\n");
                pr_printf("set ibd measures Spairs Srobdom\n");
                pr_printf("set ibd tests norm permu\n");
                pr_printf("set ibd permutations 999\n");
                if (_Top->PedCnt > 100) {
                    pr_printf("\n");
                    pr_printf(" comp COMMENT OUT THIS LINE AND THE NEXT IF YOU WANT TO RUN LM_IBDTESTS\n");
                    pr_printf(" comp BUT BE WARNED WITH THESE OPTIONS LM_IBDTEST USES MANY GIGABYTES OF MEMORY\n");
                }
            } else if (subopt == 7) { //lm_ibdtests_lr (lr == likelyhood ratio)
                pr_printf("compute likelihood-ratio stats\n");
                pr_printf("set like measures delta lambda-p\n");
                pr_printf("set like lambda-p model grid 6 9\n");
            }
        }
    } *xp2 = new lod_par_user(Top);
    xp2->file_names     = file_names;
    xp2->subopt         = subopt;
    xp2->iterate(); 
    delete xp2;

    liability_traits *xp3 = new liability_traits(Top);
    xp3->file_names     = file_names;
    xp3->iterate();
    delete xp3;
}

static double get_gp(ext_linkage_locus_top *EXLTop, int LType, int chr, char *snp, int m, int choice)
{
    double genetic_distance = 0.0;

    if (genetic_distance_index >= 0) {
        if (choice == 1) {
            // we were told to only use the average map, or only an average map was specified...
            genetic_distance = EXLTop->EXLocus[m].positions[genetic_distance_index];
        } else if (choice == 2) {
            genetic_distance = EXLTop->EXLocus[m].pos_female[genetic_distance_index];
        } else if (choice == 3) {
            genetic_distance = EXLTop->EXLocus[m].pos_male[genetic_distance_index];
        }
    }

    return genetic_distance;
}

/**
   @param output_format determines how the files are written:
*/
static void write_PANGAEA_map(linkage_ped_top *Top, char *file_names[], int subopt)
{

    vlpCLASS(PANGAEA_map_names,chr,loci) {
     vlpCTOR(PANGAEA_map_names,chr,loci) { }
        typedef char *str;
        str *file_names;
        int token;

        void file_loop() {
            mssgvf("        PANGAEA names map file:     %s/%s\n", *_opath, file_names[1]);
            data_loop(*_opath, file_names[1], "w");
        }
        void file_header() {
            pr_printf("set marker names ");
            token = 0;
        }
        void inner() {
            if (token++ >= 100) { pr_nl(); token = 1; }
            pr_printf("%s ", _tlocusp->LocusName);
        }
        void file_trailer() {
            pr_nl();
        }

    } *xp = new PANGAEA_map_names(Top);
    xp->file_names     = file_names;
    xp->iterate(); 
    delete xp;

    vlpCLASS(PANGAEA_map_dist,chr,loci) {
     vlpCTOR(PANGAEA_map_dist,chr,loci) {
            delta = .000001;
        }
        typedef char *str;
        str *file_names;
        double ogp;
        char *name;
        int choice;
        int token;
        double delta;

        void file_loop() {
            ogp = 0 - delta;
            mssgvf("        PANGAEA dist map file:      %s/%s\n", *_opath, file_names[1]);
            data_loop(*_opath, file_names[1], "a");
        }
        void file_header() {
            pr_printf("map %s marker Kosambi positions ", name);
            token = 0;
        }
        void inner () {
            int chr = _tlocusp->Marker->chromosome;
            if (chr == UNKNOWN_CHROMO || chr >= MITO_CHROMOSOME) chr = 0;

            double gp = get_gp(_EXLTop, _tlocusp->Type, _tlocusp->Marker->chromosome, _tlocusp->LocusName, _locus, choice);
//          gp = kosambi_to_haldane(gp);
//          pr_printf(" <%.6f, %.6f> ", gp, ogp);
            if (ogp > gp - delta) gp = ogp + delta;
            ogp = gp;
            if (token++ >= 100) { pr_nl(); token = 1; }
            pr_printf("%.6f ", gp);
        }
        void file_trailer() {
            pr_nl();
        }
    };

    if (genetic_distance_sex_type_map == SEX_AVERAGED_MAP) {
        PANGAEA_map_dist *xp1 = new PANGAEA_map_dist(Top);
        xp1->file_names = file_names;
        xp1->name       = (char *)"";
        xp1->choice     = 1;
        xp1->iterate();
        delete xp1;
    } else {
        PANGAEA_map_dist *xp1 = new PANGAEA_map_dist(Top);
        xp1->file_names = file_names;
        xp1->name       = (char *)"gender f";
        xp1->choice     = 2;
        xp1->iterate();
        delete xp1;

        xp1 = new PANGAEA_map_dist(Top);
        xp1->file_names = file_names;
        xp1->name       = (char *)"gender m";
        xp1->choice     = 3;
        xp1->iterate();
        delete xp1;
    }

    vlpCLASS(PANGAEA_map_freq,chr,loci) {
     vlpCTOR(PANGAEA_map_freq,chr,loci) { i = 0; }
        typedef char *str;
        str *file_names;
        int i;
        int token;

        void file_loop() {
            mssgvf("        PANGAEA freq map file:      %s/%s\n", *_opath, file_names[1]);
            data_loop(*_opath, file_names[1], "a");
        }
        void chr_start() { i = 0; }
        void inner() {
            int j;
            pr_printf("set marker %d allele freqs ", ++i);
            linkage_allele_rec *allele_info = _tlocusp->Allele;
            for (j = 0; j < _tlocusp->AlleleCnt; j++) {
                pr_printf("%.4f ", allele_info[j].Frequency);
            }
            pr_nl();
        }
    } *xp2 = new PANGAEA_map_freq(Top);
    xp2->file_names     = file_names;
    xp2->iterate(); 
    delete xp2;

    vlpCLASS(PANGAEA_map_data,chr,ped_per_loci) {
     vlpCTOR(PANGAEA_map_data,chr,ped_per_loci) { }
        typedef char *str;
        str *file_names;
        int token;
        int subopt;
        int skip;

        void file_loop() {
            mssgvf("        PANGAEA data map file:      %s/%s\n", *_opath, file_names[1]);
            data_loop(*_opath, file_names[1], "a");
        }
        void file_header() { 
            int i, j = NumChrLoci;
            for (i = 0; i < num_traits; i++) { // num_traits as a count includes space for -1 and -99
                if (global_trait_entries[i] != -1 && global_trait_entries[i] != -99) j--;
            }
            pr_printf("set MARKERS %d data\n", j);  // true count of markers
            token = 0;
        }
        void ped_start() {
            if (subopt == 6) {
/*
                if (_tpedtreep->EntryCnt > 50 || _ped > 100) {
                    skip = 1;
                } else
*/
                    skip = 0;
            } else
                skip = 0;
        }
        void per_start() {
            if (skip) return;
            pr_per();
        }
        void inner () {
            if (skip) return;
            if (token++ >= 100) { pr_nl(); token = 1; }
            pr_marker();
        }
        void per_end() {
            if (skip) return;
            pr_nl();
        }
    } *xp3 = new PANGAEA_map_data(Top);
    xp3->file_names     = file_names;
    xp3->subopt         = subopt;
    xp3->load_formats(5, 5, -1);   //?
    xp3->iterate(); 
    delete xp3;
}

void CLASS_PANGAEA::create_output_file(
    linkage_ped_top *LPedTreeTop,
    analysis_type *analysis,
    char *file_names[],
    int untyped_ped_opt,
    int *numchr,
    linkage_ped_top **Top2)
{
    linkage_ped_top *Top = LPedTreeTop;
    int combine_chromo = 0;

    batch_in();
    combine_chromo = !LoopOverChrm;

    get_file_names(Top->OrigIds, Top->UniqueIds, &combine_chromo);
    combine_chromo = 0;  // override ... requires 1 chrm per file
    LoopOverChrm   = !combine_chromo;

    set_file_names(LPedTreeTop, file_names, &combine_chromo);

    run_analysis(LPedTreeTop, analysis, file_names, untyped_ped_opt, numchr, Top2);

}

void CLASS_PANGAEA::run_analysis(
    linkage_ped_top *LPedTreeTop,
    analysis_type *analysis,
    char *file_names[],
    int untyped_ped_opt,
    int *numchr,
    linkage_ped_top **Top2)
{
    linkage_ped_top *Top = LPedTreeTop;
    char sub_prog[32];

    int pwid, fwid, mwid;

    // if 'combine_chromo == 0' each chromosome gets it's own file.
    // if 'combine_chromo == 1' all informaiton goes into one file with the '.all' suffix.

    // the analysis parameter is not used by this function...
    create_mssg(*analysis);
    
    // determine the maximum field widths necessary to output certain data...
    field_widths(Top, Top->LocusTop, &fwid, &pwid, NULL, &mwid);
    
    // Omit pedigrees under certain circumstances...
    omit_peds(untyped_ped_opt, Top);

    sub_prog_name(_suboption, sub_prog);

    save_PANGAEA_peds(Top, file_names, pwid, fwid, _suboption);

    if (_suboption > 2) {
        // need trait ...
        if (! have_trait_b(Top->LocusTop, /*affect=*/true, /*quant=*/false)) {
            errorvf("Morgan option %s requires that a trait be specified.\n", sub_prog);
            EXIT(DATA_INCONSISTENCY);
        }
        write_PANGAEA_map(Top, file_names, _suboption);
    }

#if 0
    sub_prog_name(_suboption, prefix);
    sprintf(file_names[3], "%s..sh", prefix);
    sprintf(file_names[4], "%s.top.sh", prefix);
    sprintf(file_names[5], "%s..", prefix);
    sprintf(file_names[6], "%s", prefix);
    sprintf(file_names[7], "%s..par_chr_trt", prefix);
    sprintf(file_names[8], "%s..par_user", prefix);
    sprintf(file_names[9], "%s.par_var", prefix);
    sprintf(file_names[10], "%s.par_trt", prefix);
    sprintf(file_names[11], "..dat");
#endif
    write_PANGAEA_sh(Top, file_names, sub_prog, _suboption);

    switch (_suboption) {
    case 0:
    case 1:  write_PANGAEA_par_pedcheck(Top, file_names, sub_prog);   break;
    case 2:  write_PANGAEA_par_kin(Top, file_names, sub_prog);   break;
    case 3:  write_PANGAEA_par_translink(Top, file_names, sub_prog);   break;
    case 4:  write_PANGAEA_par_lod(Top, file_names, sub_prog, _suboption);   break;
    case 5:  write_PANGAEA_par_lod(Top, file_names, sub_prog, _suboption);   break;
    case 6:  write_PANGAEA_par_ibd_tests(Top, file_names, sub_prog, _suboption);   break;
    case 7:  write_PANGAEA_par_ibd_tests(Top, file_names, sub_prog, _suboption);   break;
    default:
        break;

/* so compiler won't complain, "use" template */
    case -99:  write_PANGAEA_par_template(Top, file_names, sub_prog);   break;
    }
}

const char *sub_prog_string(int sub_opt) {
    switch(sub_opt) {
    case 0:
    case 1:  return "pangaea_pedcheck";              break;
    case 2:  return "pangaea_kin";                   break;
    case 3:  return "pangaea_translink";             break;
    case 4:  return "pangaea_lod_lm_linkage";        break;
    case 5:  return "pangaea_lod_lm_bayes";          break;
    case 6:  return "pangaea_lm_ibdtests";           break;
    case 7:  return "pangaea_lm_ibdtests_lr";        break;
    default: return "pangaea_???";                   break;
    }
}

void CLASS_PANGAEA::sub_prog_name(int sub_opt, char *subprog) {
    switch(sub_opt) {
    case 0:
    case 1:  strcpy(subprog, "pedcheck");              break;
    case 2:  strcpy(subprog, "kin");                   break;
    case 3:  strcpy(subprog, "translink");             break;
    case 4:  strcpy(subprog, "lm_linkage");            break;
    case 5:  strcpy(subprog, "lm_bayes");              break;
    case 6:  strcpy(subprog, "lm_ibdtests");           break;
    case 7:  strcpy(subprog, "lm_ibdtests_lr");        break;
    default:                                           break;
    }
}

void CLASS_PANGAEA::interactive_sub_prog_name_to_sub_option(analysis_type *analysis)
{
    int selection = 1;
    int selected  = 1;
    char select[10];

    if (batchANALYSIS) {
        if (Mega2BatchItems[/* 6 */ Analysis_Sub_Option].items_read) {
            selection = (*analysis)->_suboption;
        } else {
            selection = 1;
        }
    } else {
        while (selected != 0) {
            draw_line();
            printf("Selection Menu: PANGAEA output file options\n");
            printf("0) Done with this menu - please proceed\n");
            printf("%c1) generate PANGAEA pedcheck Output files\n",
                   selection == 1 ? '*' : ' ');
            printf("%c2) generate PANGAEA kinship Output files\n",
                   selection == 2 ? '*' : ' ');
            printf("%c3) generate PANGAEA translink Output files\n",
                   selection == 3 ? '*' : ' ');
            printf("%c4) generate PANGAEA LOD lm_linkage Output files\n",
                   selection == 4 ? '*' : ' ');
            printf("%c5) generate PANGAEA LOD lm_bayes Output files\n",
                   selection == 5 ? '*' : ' ');
            printf("%c6) generate PANGAEA lm_ibdtests Output files\n",
                   selection == 6 ? '*' : ' ');
            printf("%c7) generate PANGAEA lm_ibdtests_lr Output files\n",
                   selection == 7 ? '*' : ' ');
            printf("Enter selection: 0 - 7 > ");
            fcmap(stdin,"%s", select); newline;
            sscanf(select, "%d", &selected);
            if (selected < 0 || selected > 7) warn_unknown(select);
            else if (selected) selection = selected;
        }
    }
    (*analysis)->_suboption = selection;
    free((*analysis)->file_name_stem);
    (*analysis)->file_name_stem = strdup(sub_prog_string((*analysis)->_suboption));
}

void CLASS_PANGAEA::sub_prog_name_to_sub_option(char *sub_prog_name, analysis_type *analysis) {
    switch(tolower((unsigned char)sub_prog_name[0])) {
    case 'p': // pedcheck
        (*analysis)->_suboption = 1; break;
    case 'k': // kinship
        (*analysis)->_suboption = 2; break;
    case 't': // translink
        (*analysis)->_suboption = 3; break;
    case 0: // missing
//      (*analysis)->_suboption = 1; break;
        interactive_sub_prog_name_to_sub_option(analysis); break;

    default:
        switch(tolower((unsigned char)sub_prog_name[3])) {
        case 'l': // lm_linkage
            (*analysis)->_suboption = 4; break;
        case 'b': // lm_bayes
            (*analysis)->_suboption = 5; break;
        case 'i': // lm_ibdtests
        default:
            switch(strlen(sub_prog_name)) {
            case 11:
                (*analysis)->_suboption = 6; break;
            case 14:
                (*analysis)->_suboption = 7; break;
            }
        }
        break;
    }
    free((*analysis)->file_name_stem);
    (*analysis)->file_name_stem = strdup(sub_prog_string((*analysis)->_suboption));

}

void CLASS_PANGAEA::get_file_names(int has_orig, int has_uniq, int *combine_chromo)
{
    int i, choice;
    int igl, ipre, iphen, ish, ioui, ioup, isum, isumf;
    char prefix[MAX_NAMELEN];
    analysis_type analysis = this;

    if (DEFAULT_OUTFILES || (InputMode != INTERACTIVE_INPUTMODE)) {
        mssgf("Output file names set to defaults.");
        choice = 0;
    } else {
        choice = -1;
    }
    if (main_chromocnt > 1) {
        // This is the batch file item that controls whether you wish to comnine the
        // chromosomes in the same file or not. If true (y), each chromosome gets it's own file.
        // This is a derective from the user which will override the default...
        if (Mega2BatchItems[/* 50 */ Loop_Over_Chromosomes].items_read)
            *combine_chromo = (tolower((unsigned char)Mega2BatchItems[/* 50 */ Loop_Over_Chromosomes].value.copt) == 'y') ? 0 : 1;
        else
            *combine_chromo=0;
    }

    /* output file name menu */
    igl = ipre = iphen = ish = ioui = ioup = isum = isumf = -1;
    // If the default output files are used we do not go here.
    // Otherwise, enter with choice -- -1.
    while (choice != 0) {
        draw_line();
        print_outfile_mssg();
        printf("Output file names menu:\n");
        printf("0) Done with this menu - please proceed\n");
        i=1;

        if (main_chromocnt > 1) {
            printf(" %d) Combine chromosomes?                      %s\n",
                   i, yorn[*combine_chromo]);
            igl=i++;
        }

        printf(" %d) File name stem:                      %-15s\n", i, file_name_stem);
        ipre=i++;

        individual_id_item(i, analysis, OrigIds[0], 43, 2, 0, 0);
        ioui=i++;

        pedigree_id_item(i, analysis, OrigIds[1], 43, 2, 0);
        ioup=i;

        printf("Enter options 0-%d > ", i);
        fcmap(stdin, "%d", &choice); printf("\n");
        test_modified(choice);

        if (choice < 0) {
            printf("Unknown option %d\n", choice);
        } else if (choice == 0) {
            ;

        } else if (choice == igl) {
            *combine_chromo = TOGGLE(*combine_chromo);
#if 0
            if (main_chromocnt > 1 && *combine_chromo) {
                // replaces <extension> with 'all', keeping <extension> and <rest> if they exist...
                analysis->replace_chr_number(file_names, 0);
            } else {
                analysis->replace_chr_number(file_names, global_chromo_entries[0]);
            }
#endif
        } else if (choice == ipre) {
            printf("Enter new file name stem > ");
            fcmap(stdin, "%s", prefix);    newline;
            free(file_name_stem);
            file_name_stem = strdup(prefix);
            BatchValueSet(file_name_stem, "file_name_stem");
            batchf("file_name_stem");

        } else if (choice == ioui) {
            OrigIds[0] = individual_id_item(0, analysis, OrigIds[0], 35, 1, has_orig, has_uniq);
            individual_id_item(0, analysis, OrigIds[0], 0, 3, has_orig, has_uniq);

        } else if (choice == ioup) {
            OrigIds[1] = pedigree_id_item(0, analysis, OrigIds[1], 35, 1, has_orig);
            pedigree_id_item(0, analysis, OrigIds[1], 0, 3, has_orig);

        } else {
            printf("Unknown option %d\n", choice);
        }
    }
}

void CLASS_PANGAEA::set_file_names(linkage_ped_top *LPedTreeTop, char *file_names[], int *combine_chromo)
{
    batch_show();

    extern void set_file_names_and_paths(const analysis_type analysis,
                                         linkage_ped_top *LPedTreeTop);

    set_file_names_and_paths(this, LPedTreeTop);
    if (main_chromocnt > 1 && *combine_chromo)
        replace_chr_number(file_names, 0);
    else
        replace_chr_number(file_names, global_chromo_entries[0]);
}

void CLASS_PANGAEA::batch_in()
{
//  char c;

//  BatchValueGet(additional_program_args, "additional_program_args");

//  BatchValueGet(c, "Loop_Over_Chromosomes");
//  LoopOverChrm = (c == 'y' || c == 'Y');

    file_name_stem = strdup(sub_prog_string(_suboption));
    batch_item_type *bi = BatchItemGet("file_name_stem");
    if (bi->items_read) {
        free(file_name_stem);
        file_name_stem = strdup(bi->value.name);
    }
}

void CLASS_PANGAEA::batch_show()
{
    msgvf("\n");
    msgvf("Output file stem:                       %s\n",    C(file_name_stem));
    
//  msgvf("Additional PANGAEA program args:        %s\n",
//        (additional_program_args.size() > 0 ? C(additional_program_args) : 
//          "<none specified>"));

    msgvf("\n");
}

static void inner_file_names(char **file_names, const char *num, const char *stem /* = "pangaea" */) {

    sprintf(file_names[0], "%s.ped", stem);
    sprintf(file_names[1], "%s.%s.map", stem, num);
    sprintf(file_names[3], "%s.%s.sh", stem, num);
    sprintf(file_names[4], "%s.top.sh", stem);
    sprintf(file_names[5], "%s.%s.", stem, num);
    sprintf(file_names[6], "%s", stem);
    sprintf(file_names[7], "%s.%s.par_chr_trt", stem, num);
    sprintf(file_names[8], "%s.%s.par_user", stem, num);
    sprintf(file_names[9], "%s.all.ped", stem);
    sprintf(file_names[10], "%s.par_trt", stem);
    sprintf(file_names[11], ".%s.dat", num);
}

void CLASS_PANGAEA::gen_file_names(char **file_names, char *num)
{
    inner_file_names(file_names, num, file_name_stem);
}

void CLASS_PANGAEA::replace_chr_number(char *file_names[], int numchr) {
    change_output_chr(file_names[1], numchr);
    change_output_chr(file_names[3], numchr);
    change_output_chr(file_names[5], numchr);
    change_output_chr(file_names[7], numchr);
    change_output_chr(file_names[8], numchr);
    change_output_chr(file_names[11], numchr);
}
