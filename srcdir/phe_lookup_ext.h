/*
  Mega2: Manipulation Environment for Genetic Analysis.

  Copyright 1999-2019, University of Pittsburgh. All Rights Reserved.

  Contributors to Mega2: Robert Baron, Justin R. Stickel, Charles P. Kollar,
  Nandita Mukhopadhyay, Lee Almasy, Mark Schroeder, William P. Mulvihill,
  and Daniel E. Weeks.

  This file is part of the Mega2 program, which is free software; you
  can redistribute it and/or modify it under the terms of the GNU
  General Public License as published by the Free Software Foundation;
  either version 3 of the License, or (at your option) any later
  version.

  Mega2 is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  For further information contact:
      Daniel E. Weeks
      e-mail: weeks@pitt.edu

===========================================================================
*/

#ifndef PHE_LOOKUP_EXT_H
#define PHE_LOOKUP_EXT_H

#include <string>
#include <map>

//
// For mapping the VCF Tools sample into the pedigree,person found in this file.
typedef std::map<std::string, std::pair<std::string,std::string> > sample_map_type;
typedef std::map<std::pair<std::string,std::string>,std::string>  pedper_map_type;
extern sample_map_type *SAMPLEIDS;
extern pedper_map_type *PEDPERIDS;

extern void phefree();

extern void phehdr(char **names, int *types);

extern int  phemake(const char *s);

extern int  phesearch(const char *ped, const char *per, char **out);

#endif
